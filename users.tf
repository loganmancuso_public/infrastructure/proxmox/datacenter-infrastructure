##############################################################################
#
# Author: Logan Mancuso
# Created: 04.04.2024
#
##############################################################################

### Users and Groups ###
resource "proxmox_virtual_environment_group" "operations" {
  comment  = "Terraform Deployment Ops Team"
  group_id = "team-operations"
}

resource "proxmox_virtual_environment_role" "operations" {
  role_id    = "role-operations"
  privileges = data.proxmox_virtual_environment_roles.available_roles.privileges[0]
}

resource "proxmox_virtual_environment_user" "operations" {
  acl {
    path      = "/"
    propagate = true
    role_id   = proxmox_virtual_environment_role.operations.role_id
  }
  groups   = [proxmox_virtual_environment_group.operations.group_id]
  comment  = "Terraform Deployment User"
  password = local.credentials_operations.password
  user_id  = local.credentials_operations.username
}
