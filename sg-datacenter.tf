##############################################################################
#
# Author: Logan Mancuso
# Created: 04.04.2024
#
##############################################################################

### Datacenter Default Rules ###
resource "proxmox_virtual_environment_cluster_firewall_security_group" "dc_default" {
  name    = "sg-datacenter"
  comment = "Default Datacenter Security Group"

  ### Inbound Rules ###

  ## ProxmoxUI and API ##
  rule {
    type    = "in"
    action  = "ACCEPT"
    comment = "proxmoxui ${proxmox_virtual_environment_firewall_ipset.trusted.name} >> ${proxmox_virtual_environment_firewall_ipset.nodes.name}"
    source  = "+${proxmox_virtual_environment_firewall_ipset.trusted.id}"
    dest    = "+${proxmox_virtual_environment_firewall_ipset.nodes.id}"
    dport   = "8006"
    proto   = "tcp"
    log     = "alert"
  }

  ## ProxmoxUI and API, accessable to the node itself for packers use##
  rule {
    type    = "in"
    action  = "ACCEPT"
    comment = "proxmoxui self ${proxmox_virtual_environment_firewall_ipset.nodes.name}"
    source  = "+${proxmox_virtual_environment_firewall_ipset.nodes.id}"
    dest    = "+${proxmox_virtual_environment_firewall_ipset.nodes.id}"
    dport   = "8006"
    proto   = "tcp"
    log     = "alert"
  }

  ## Packer Build ##
  rule {
    type    = "in"
    action  = "ACCEPT"
    comment = "packer ${proxmox_virtual_environment_firewall_ipset.vpc_networks.name} >> ${proxmox_virtual_environment_firewall_ipset.nodes.name}"
    source  = "+${proxmox_virtual_environment_firewall_ipset.vpc_networks.id}"
    dest    = "+${proxmox_virtual_environment_firewall_ipset.nodes.id}"
    dport   = "8800:8810"
    proto   = "tcp"
    log     = "alert"
  }
  # packer from each node to vpc network
  rule {
    type    = "in"
    action  = "ACCEPT"
    comment = "packer ${proxmox_virtual_environment_firewall_ipset.nodes.name} >> ${proxmox_virtual_environment_firewall_ipset.vpc_networks.name}"
    source  = "+${proxmox_virtual_environment_firewall_ipset.nodes.id}"
    dest    = "+${proxmox_virtual_environment_firewall_ipset.vpc_networks.id}"
    dport   = "8800:8810"
    proto   = "tcp"
    log     = "alert"
  }

  ## SSH ##
  # ssh to each node
  rule {
    type    = "in"
    action  = "ACCEPT"
    comment = "ssh ${proxmox_virtual_environment_firewall_ipset.trusted.name}  >> ${proxmox_virtual_environment_firewall_ipset.nodes.name}"
    source  = "+${proxmox_virtual_environment_firewall_ipset.trusted.id}"
    dest    = "+${proxmox_virtual_environment_firewall_ipset.nodes.id}"
    dport   = "22"
    proto   = "tcp"
    log     = "alert"
  }
  # ssh to all internal vpc networks
  rule {
    type    = "in"
    action  = "ACCEPT"
    comment = "ssh ${proxmox_virtual_environment_firewall_ipset.trusted.name}  >> ${proxmox_virtual_environment_firewall_ipset.vpc_networks.name}"
    source  = "+${proxmox_virtual_environment_firewall_ipset.trusted.id}"
    dest    = "+${proxmox_virtual_environment_firewall_ipset.vpc_networks.id}"
    dport   = "22"
    proto   = "tcp"
    log     = "alert"
  }
  # permit node to ssh into instances hosted on it
  rule {
    type    = "in"
    action  = "ACCEPT"
    comment = "ssh ${proxmox_virtual_environment_firewall_ipset.nodes.name} >> ${proxmox_virtual_environment_firewall_ipset.vpc_networks.name}"
    source  = "+${proxmox_virtual_environment_firewall_ipset.nodes.id}"
    dest    = "+${proxmox_virtual_environment_firewall_ipset.vpc_networks.id}"
    dport   = "22"
    proto   = "tcp"
    log     = "alert"
  }

  ## ICMP ##
  # icmp to each node
  rule {
    type    = "in"
    action  = "ACCEPT"
    comment = "icmp ${proxmox_virtual_environment_firewall_ipset.trusted.name} >> ${proxmox_virtual_environment_firewall_ipset.nodes.name}"
    source  = "+${proxmox_virtual_environment_firewall_ipset.trusted.id}"
    dest    = "+${proxmox_virtual_environment_firewall_ipset.nodes.id}"
    proto   = "icmp"
    log     = "alert"
  }
  # icmp to all internal vpc networks
  rule {
    type    = "in"
    action  = "ACCEPT"
    comment = "icmp ${proxmox_virtual_environment_firewall_ipset.trusted.name} >> ${proxmox_virtual_environment_firewall_ipset.vpc_networks.name}"
    source  = "+${proxmox_virtual_environment_firewall_ipset.trusted.id}"
    dest    = "+${proxmox_virtual_environment_firewall_ipset.vpc_networks.id}"
    proto   = "icmp"
    log     = "alert"
  }
  # permit node to ping instances hosted on it
  rule {
    type    = "in"
    action  = "ACCEPT"
    comment = "icmp ${proxmox_virtual_environment_firewall_ipset.nodes.name} >> ${proxmox_virtual_environment_firewall_ipset.vpc_networks.name}"
    source  = "+${proxmox_virtual_environment_firewall_ipset.nodes.id}"
    dest    = "+${proxmox_virtual_environment_firewall_ipset.vpc_networks.id}"
    proto   = "icmp"
    log     = "alert"
  }

  ### Outbound Rules ###

  ## DNS ##
  # dns for cluster nodes
  rule {
    type    = "out"
    action  = "ACCEPT"
    comment = "dns ${proxmox_virtual_environment_firewall_ipset.nodes.name} >> ${proxmox_virtual_environment_firewall_ipset.dns.name}"
    source  = "+${proxmox_virtual_environment_firewall_ipset.nodes.id}"
    dest    = "+${proxmox_virtual_environment_firewall_ipset.dns.id}"
    proto   = "tcp"
    dport   = 53
    log     = "alert"
  }
  rule {
    type    = "out"
    action  = "ACCEPT"
    comment = "dns ${proxmox_virtual_environment_firewall_ipset.nodes.name} >> ${proxmox_virtual_environment_firewall_ipset.dns.name}"
    source  = "+${proxmox_virtual_environment_firewall_ipset.nodes.id}"
    dest    = "+${proxmox_virtual_environment_firewall_ipset.dns.id}"
    proto   = "udp"
    dport   = 53
    log     = "alert"
  }
  # dns for vpc networks
  rule {
    type    = "out"
    action  = "ACCEPT"
    comment = "dns ${proxmox_virtual_environment_firewall_ipset.vpc_networks.name} >> ${proxmox_virtual_environment_firewall_ipset.dns.name}"
    source  = "+${proxmox_virtual_environment_firewall_ipset.vpc_networks.id}"
    dest    = "+${proxmox_virtual_environment_firewall_ipset.dns.id}"
    proto   = "tcp"
    dport   = 53
    log     = "alert"
  }
  rule {
    type    = "out"
    action  = "ACCEPT"
    comment = "dns ${proxmox_virtual_environment_firewall_ipset.vpc_networks.name} >> ${proxmox_virtual_environment_firewall_ipset.dns.name}"
    source  = "+${proxmox_virtual_environment_firewall_ipset.vpc_networks.id}"
    dest    = "+${proxmox_virtual_environment_firewall_ipset.dns.id}"
    proto   = "udp"
    dport   = 53
    log     = "alert"
  }
  # ntp for cluster nodes
  rule {
    type    = "out"
    action  = "ACCEPT"
    comment = "ntp ${proxmox_virtual_environment_firewall_ipset.nodes.name} >> ${proxmox_virtual_environment_firewall_ipset.dns.name}"
    source  = "+${proxmox_virtual_environment_firewall_ipset.nodes.id}"
    dest    = "+${proxmox_virtual_environment_firewall_ipset.dns.id}"
    proto   = "udp"
    dport   = 123
    log     = "alert"
  }
  # ntp for vpc networks
  rule {
    type    = "out"
    action  = "ACCEPT"
    comment = "ntp ${proxmox_virtual_environment_firewall_ipset.vpc_networks.name} >> ${proxmox_virtual_environment_firewall_ipset.dns.name}"
    source  = "+${proxmox_virtual_environment_firewall_ipset.vpc_networks.id}"
    dest    = "+${proxmox_virtual_environment_firewall_ipset.dns.id}"
    proto   = "udp"
    dport   = 123
    log     = "alert"
  }
}
