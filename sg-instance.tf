##############################################################################
#
# Author: Logan Mancuso
# Created: 04.04.2024
#
##############################################################################

### VM/LXC Default Rules ###
resource "proxmox_virtual_environment_cluster_firewall_security_group" "vm_default" {
  name    = "sg-vmdefault"
  comment = "Default VM Security Group"

  ### Inbound Rules ###
  ## SSH ## 
  # ssh to all internal vpc networks
  rule {
    type    = "in"
    action  = "ACCEPT"
    comment = "ssh ${proxmox_virtual_environment_firewall_ipset.trusted.name}  >> ${proxmox_virtual_environment_firewall_ipset.vpc_networks.name}"
    source  = "+${proxmox_virtual_environment_firewall_ipset.trusted.id}"
    dest    = "+${proxmox_virtual_environment_firewall_ipset.vpc_networks.id}"
    dport   = "22"
    proto   = "tcp"
    log     = "alert"
  }
  # permit node to ssh into instances hosted on it
  rule {
    type    = "in"
    action  = "ACCEPT"
    comment = "ssh ${proxmox_virtual_environment_firewall_ipset.nodes.name} >> ${proxmox_virtual_environment_firewall_ipset.vpc_networks.name}"
    source  = "+${proxmox_virtual_environment_firewall_ipset.nodes.id}"
    dest    = "+${proxmox_virtual_environment_firewall_ipset.vpc_networks.id}"
    dport   = "22"
    proto   = "tcp"
    log     = "alert"
  }

  ## Note
  # I dont enable this by default, this allows instances to remote into each other and that can pose a security risk.
  # I leave this here for dubugging, its useful to have side access in the network, by first remoting into an
  # existing instance then "laterally" into the instance that is having trouble booting. 
  # rule {
  #   type    = "in"
  #   action  = "ACCEPT"
  #   comment = "ssh self ${proxmox_virtual_environment_firewall_ipset.vpc_networks.name}"
  #   source  = "+${proxmox_virtual_environment_firewall_ipset.vpc_networks.id}"
  #   dest    = "+${proxmox_virtual_environment_firewall_ipset.vpc_networks.id}"
  #   dport   = "22"
  #   proto   = "tcp"
  #   log     = "alert"
  # }

  ## ICMP ##
  # icmp to all internal vpc networks
  rule {
    type    = "in"
    action  = "ACCEPT"
    comment = "icmp ${proxmox_virtual_environment_firewall_ipset.trusted.name} >> ${proxmox_virtual_environment_firewall_ipset.vpc_networks.name}"
    source  = "+${proxmox_virtual_environment_firewall_ipset.trusted.id}"
    dest    = "+${proxmox_virtual_environment_firewall_ipset.vpc_networks.id}"
    proto   = "icmp"
    log     = "alert"
  }
  # permit node to ping instances hosted on it
  rule {
    type    = "in"
    action  = "ACCEPT"
    comment = "icmp ${proxmox_virtual_environment_firewall_ipset.nodes.name} >> ${proxmox_virtual_environment_firewall_ipset.vpc_networks.name}"
    source  = "+${proxmox_virtual_environment_firewall_ipset.nodes.id}"
    dest    = "+${proxmox_virtual_environment_firewall_ipset.vpc_networks.id}"
    proto   = "icmp"
    log     = "alert"
  }

  ### Outbound Rules ###
  ## HTTP(s) ##
  rule {
    type    = "out"
    action  = "ACCEPT"
    comment = "http"
    source  = "+${proxmox_virtual_environment_firewall_ipset.vpc_networks.id}"
    dest    = "dc/${proxmox_virtual_environment_firewall_alias.networks["global"].id}"
    proto   = "tcp"
    dport   = 80
    log     = "alert"
  }
  rule {
    type    = "out"
    action  = "ACCEPT"
    comment = "https"
    source  = "+${proxmox_virtual_environment_firewall_ipset.vpc_networks.id}"
    dest    = "dc/${proxmox_virtual_environment_firewall_alias.networks["global"].id}"
    proto   = "tcp"
    dport   = 443
    log     = "alert"
  }
  ## DNS ##
  rule {
    type    = "out"
    action  = "ACCEPT"
    comment = "dns ${proxmox_virtual_environment_firewall_ipset.vpc_networks.name} >> ${proxmox_virtual_environment_firewall_ipset.dns.name}"
    source  = "+${proxmox_virtual_environment_firewall_ipset.vpc_networks.id}"
    dest    = "+${proxmox_virtual_environment_firewall_ipset.dns.id}"
    proto   = "tcp"
    dport   = 53
    log     = "alert"
  }
  rule {
    type    = "out"
    action  = "ACCEPT"
    comment = "dns ${proxmox_virtual_environment_firewall_ipset.vpc_networks.name} >> ${proxmox_virtual_environment_firewall_ipset.dns.name}"
    source  = "+${proxmox_virtual_environment_firewall_ipset.vpc_networks.id}"
    dest    = "+${proxmox_virtual_environment_firewall_ipset.dns.id}"
    proto   = "udp"
    dport   = 53
    log     = "alert"
  }
  ## NTP ##
  rule {
    type    = "out"
    action  = "ACCEPT"
    comment = "ntp ${proxmox_virtual_environment_firewall_ipset.vpc_networks.name} >> ${proxmox_virtual_environment_firewall_ipset.dns.name}"
    source  = "+${proxmox_virtual_environment_firewall_ipset.vpc_networks.id}"
    dest    = "+${proxmox_virtual_environment_firewall_ipset.dns.id}"
    proto   = "udp"
    dport   = 123
    log     = "alert"
  }
}
