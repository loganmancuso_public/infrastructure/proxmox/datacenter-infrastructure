##############################################################################
#
# Author: Logan Mancuso
# Created: 11.27.2023
#
##############################################################################

resource "proxmox_virtual_environment_cluster_options" "options" {
  language   = "en"
  keyboard   = "en-us"
  email_from = "notify@${var.nodes["primary"].name}.cluster.vpc.home.arpa"
  # bandwidth_limit_migration = 555555
  # bandwidth_limit_default   = 666666
  # max_workers               = 5
  migration_cidr = proxmox_virtual_environment_firewall_alias.networks["cluster"].cidr
  migration_type = "secure"
}

## Datacenter Cert ##
resource "tls_private_key" "datacenter" {
  algorithm = "RSA"
  rsa_bits  = 4096
}
# add keys to vault
resource "vault_kv_secret_v2" "datacenter_keys" {
  mount               = local.vault_paths["infra"]
  name                = "${var.datacenter_name}_datacenter_keys"
  cas                 = 1
  delete_all_versions = true
  data_json = jsonencode({
    private = tls_private_key.datacenter.private_key_pem
    public  = tls_private_key.datacenter.public_key_pem
  })
}