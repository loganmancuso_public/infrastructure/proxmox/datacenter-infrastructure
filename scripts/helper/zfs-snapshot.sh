#!/bin/bash
##############################################################################
#
# Author: Logan Mancuso
# Created: 03.13.2024
#
##############################################################################

# Redirect all output to log file
exec > >(tee "/var/log/tofu/zfs-snapshot.log") 2>&1

function clean_snapshots() {
  local pool=$1
  echo -e "END:\tclean_snapshots"
  # Calculate the timestamp in seconds
  local date=$(date -d "6 months ago" +%s)
  # List all snapshots for the given ZFS target and filter by creation time
  zfs list -t snapshot -o name,creation -H -r "$pool" | while read -r snapshot_name creation; do
    # Convert snapshot creation time to seconds since epoch
    timestamp=$(date -d "$creation" +%s)
    # Compare snapshot creation time with the timestamp of 6 months ago
    if [ "$timestamp" -lt "$date" ]; then
      # Delete snapshots older than time
      echo -e "Deleting snapshot\t$snapshot_name"
      echo -e "Created on\t$creation"
      zfs destroy "$snapshot_name"
    fi
  done
  echo -e "END:\tclean_snapshots"
}

# backup function
function backup() {
  local pool=$1
  echo -e "START:\tbackup"
  local datasets=$(zfs list -H -o name -r "$pool")
  # Iterate through each dataset and create a snapshot
  for dataset in $datasets; do
    # Generate a snapshot name with the current date/time
    name=$(date +"%Y%m%d%H%M%S")
    # Create the snapshot
    zfs snapshot "$dataset@$name"
    zfs list -t snapshot -r $pool
  done
  clean_snapshots $pool
  echo -e "END:\tbackup"
}

# Function to display usage
function usage() {
  echo "Usage: $0 -zfs [--pool name]"
  exit 1
}

# Main function
function main() {
  # Parse arguments using getopts with short and long options
  while [[ $# -gt 0 ]]; do
    case "$1" in
      -zfs|--pool)
        pool="$2"
        shift 2
        ;;
      -h|--help)
        usage
        ;;
      *)
        echo "Unknown option: $1"
        usage
        ;;
    esac
  done
  echo -e "START:\tmain"
  # run the backup function
  backup $pool
  echo -e "END:\tmain"
}

# Start the script
main "$@"