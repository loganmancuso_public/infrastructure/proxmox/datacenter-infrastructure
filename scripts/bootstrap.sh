#!/bin/bash
##############################################################################
#
# Author: Logan Mancuso
# Created: 11.10.2023
#
##############################################################################

# Redirect all output to log file
exec > >(tee -a "${log}") 2>&1

# Helper function
function helper() {
  echo -e "START:\thelper"
  # copy instance private key to node for remoting into instances
  echo "${instance_key}" > "$HOME/.ssh/instance_key"
  # copy resources private key to node for accessing hosted resources
  echo "${resources_key}" > "$HOME/.ssh/resources_key"
  # copy authorized public key for authorized ssh access to the node
  local authorized_key_file="$HOME/.ssh/authorized_sshkey.pub"
  echo "${authorized_key}" > $authorized_key_file
  local authorized_keys="$HOME/.ssh/authorized_keys"
  # Check if the key exists in the authorized_keys file
  if grep -qF "$(< "$authorized_key_file")" "$authorized_keys"; then
    echo "Key already exists in authorized_keys"
  else
    # Add the key to the authorized_keys file
    cat "$authorized_key_file" >> "$authorized_keys"
    echo "Key added to authorized_keys"
  fi
  # fix ssh key permissions #
  chmod 700 $HOME/.ssh
  chmod 600 $HOME/.ssh/instance_key
  chmod 600 $HOME/.ssh/resources_key
  chmod 644 $authorized_key_file

  ## SSH Banner ##
  sed -i '/^#Banner/s/^#//' /etc/ssh/sshd_config && sed -i '/^Banner/c\Banner /etc/ssh/ssh_banner' /etc/ssh/sshd_config
  tee /etc/ssh/ssh_banner << EOF
### Node ID: ${id} ###
EOF

  # Update Prompt #
  desired_ps1="PS1='\[\e[34m\]\u@\[\e[35m\]\h \[\e[35m\]\w \[\e[0m\]'"
  # Check if the desired PS1 is already present in the .bashrc file
  if ! grep -qF "$desired_ps1" $HOME/.bashrc; then
    # If not present, append it to the .bashrc file
    echo "$desired_ps1" >> $HOME/.bashrc
  fi
  # Remove pve subscription
  rm -f /etc/apt/sources.list.d/pve-enterprise.list /etc/apt/sources.list.d/pve-no-subscription.list
  # Remove prod ceph subscription
  rm -f /etc/apt/sources.list.d/ceph.list
  apt list --upgradable
  apt -y update
  apt -y upgrade
  # install helper tools
  apt install -y \
  git \
  curl \
  unzip \
  neovim \
  python3-full \
  python3-pip \
  ca-certificates \
  lshw \
  libsasl2-modules \
  mailutils \
  bash-completion \
  jq

  # remove pve notification of missing subscription
  curl -Lo /opt/tofu/pve-nag.sh https://raw.githubusercontent.com/foundObjects/pve-nag-buster/master/install.sh 
  chmod +x /opt/tofu/pve-nag.sh && /opt/tofu/pve-nag.sh
  echo "source /etc/network/interfaces.d/*" >> /etc/network/interfaces

  # Install Packer for Building Images
  rm -f /usr/share/keyrings/hashicorp-archive-keyring.gpg
  wget -O- https://apt.releases.hashicorp.com/gpg | gpg --dearmor -o /usr/share/keyrings/hashicorp-archive-keyring.gpg
  echo "deb [signed-by=/usr/share/keyrings/hashicorp-archive-keyring.gpg] https://apt.releases.hashicorp.com $(. /etc/os-release && echo $VERSION_CODENAME) main" | tee /etc/apt/sources.list.d/hashicorp.list
  apt update -y
  apt install -y packer

  ## Configure notifications ##

  tee /etc/pve/notifications.cfg <<EOT >/dev/null
matcher: default-matcher
  target mail-to-root
  comment default matcher

smtp: mail-to-root
  author ${node_name}
  comment ${node_name} default email notification
  from-address ${email_from}
  mailto ${email_to}
  mode starttls
  server ${smtp_server}
  username ${smtp_user}
EOT


  tee /etc/pve/priv/notifications.cfg <<EOT >/dev/null
smtp: mail-to-root
	password ${smtp_pass}
EOT


  # add zfs snapshot to systemd as a service
  if [ "${pool}" != "null" ]; then

  tee /etc/systemd/system/zfs-snapshot.service << EOF
[Unit]
Description=Run ZFS snapshot script
After=network.target

[Service]
Type=oneshot
ExecStart=/opt/tofu/helper/zfs-snapshot.sh --pool ${pool}
EOF

  tee /etc/systemd/system/zfs-snapshot.timer << EOF
[Unit]
Description=Run ZFS snapshot script every 3 hours

[Timer]
OnCalendar=*-*-* *:00:00
Unit=zfs-snapshot.service
AccuracySec=1s

[Install]
WantedBy=timers.target
EOF

    systemctl daemon-reload
    systemctl enable zfs-snapshot.timer
    systemctl start zfs-snapshot.timer
    systemctl list-timers --all

  fi

  echo -e "END:\thelper"
}

# Main function
function main() {
  echo -e "START:\tmain"
  # Display system information
  export DEBIAN_FRONTEND=noninteractive
  echo "System Information:"
  echo "hostname: $(cat /etc/hostname)"
  echo "cpu: $(top -bn1 | grep "Cpu(s)" | sed "s/.*, *\([0-9.]*\)%* id.*/\1/" | awk '{print 100 - $1"%"}')"
  echo "memory: $(free | grep Mem | awk '{print $3/$2 * 100.0"%"}')"
  echo "user: $(whoami)"
  echo "time: $(date)"
  helper
  echo -e "END:\tmain"
}

# Start the script
main "$@"