##############################################################################
#
# Author: Logan Mancuso
# Created: 04.04.2024
#
##############################################################################

data "proxmox_virtual_environment_roles" "available_roles" {}
data "proxmox_virtual_environment_nodes" "available_nodes" {}

locals {
  available_nodes = data.proxmox_virtual_environment_nodes.available_nodes
}

data "terraform_remote_state" "global_secrets" {
  backend = "http"
  config = {
    address = "https://gitlab.com/api/v4/projects/XXXXXXXX/terraform/state/global-secrets"
  }
}

locals {
  # global_secrets
  secret_cert_root  = data.terraform_remote_state.global_secrets.outputs.cert_root
  secret_proxmox    = data.terraform_remote_state.global_secrets.outputs.proxmox
  secret_operations = data.terraform_remote_state.global_secrets.outputs.user_operations
  vault_paths       = data.terraform_remote_state.global_secrets.outputs.paths
  secret_smtp       = data.terraform_remote_state.global_secrets.outputs.smtp
}

## Obtain Vault Secrets ##
data "vault_kv_secret_v2" "cert_root" {
  mount = local.secret_cert_root.mount
  name  = local.secret_cert_root.name
}

data "vault_kv_secret_v2" "proxmox" {
  mount = local.secret_proxmox.mount
  name  = local.secret_proxmox.name
}

data "vault_kv_secret_v2" "operations" {
  mount = local.secret_operations.mount
  name  = local.secret_operations.name
}

data "vault_kv_secret_v2" "smtp" {
  mount = local.secret_smtp.mount
  name  = local.secret_smtp.name
}

locals {
  cert_root              = jsondecode(data.vault_kv_secret_v2.cert_root.data_json)
  credentials_proxmox    = jsondecode(data.vault_kv_secret_v2.proxmox.data_json)
  credentials_operations = jsondecode(data.vault_kv_secret_v2.operations.data_json)
  credentials_smtp       = jsondecode(data.vault_kv_secret_v2.smtp.data_json)
  # Append Global Alias to existing network definitions #
  network_aliases = merge(var.networks, {
    "global" = {
      cidr      = "0.0.0.0"
      gateway   = "0.0.0.0"
      netmask   = "0"
      comment   = "global network cidr"
      subdomain = null
      vlan      = 0
    }
  })
  # this logic will handle a predefined vpn network and add it as part of the default trusted network. this vpn network is for inbound connections into the network
  # this is not for outbound or hosted vpn providers external to the cluster.
  is_vpn_network = contains(keys(proxmox_virtual_environment_firewall_alias.networks), "vpn") ? { vpn = proxmox_virtual_environment_firewall_alias.networks["vpn"] } : {}
}