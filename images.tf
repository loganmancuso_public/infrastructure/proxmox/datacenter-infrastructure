##############################################################################
#
# Author: Logan Mancuso
# Created: 07.04.2024
#
##############################################################################

# Lookup Table for supported Operating Systems
locals {
  ## LXC ##
  # merge and flatten this map, let _ terminate the os from the version
  flat_images_lxc = merge([
    for os, child_map in var.images.lxc : {
      for version, url in child_map : "${os}_${version}" => url
    }
  ]...)
  combined_nodes_lxc = merge([
    for image_key, image_url in local.flat_images_lxc : {
      for node_key, node_value in var.nodes : "${node_key}_${image_key}" => image_url
    }
  ]...)
  ## Virtual Machines ##
  # merge and flatten this map, let _ terminate the os from the version
  flat_images_machine = merge([
    for os, child_map in var.images.machine : {
      for version, url in child_map : "${os}_${version}" => url
    }
  ]...)
  combined_nodes_machine = merge([
    for image_key, image_val in local.flat_images_machine : {
      for node_key, node_value in var.nodes : "${node_key}_${image_key}" => image_val
    }
  ]...)
}

resource "proxmox_virtual_environment_download_file" "image_lxc" {
  for_each            = local.combined_nodes_lxc
  content_type        = "vztmpl"
  datastore_id        = "local"
  node_name           = var.nodes[split("_", each.key)[0]].name
  url                 = each.value.url
  checksum            = each.value.checksum
  checksum_algorithm  = each.value.checksum_alg
  overwrite_unmanaged = true
  overwrite           = true
  upload_timeout      = 7200 # 2 hr timeout
  lifecycle {
    prevent_destroy = true
  }
}

resource "proxmox_virtual_environment_download_file" "image_machine" {
  for_each            = local.combined_nodes_machine
  content_type        = "iso"
  datastore_id        = "local"
  node_name           = var.nodes[split("_", each.key)[0]].name
  url                 = each.value.url
  checksum            = each.value.checksum
  checksum_algorithm  = each.value.checksum_alg
  overwrite_unmanaged = true
  overwrite           = true
  upload_timeout      = 7200 # 2 hr timeout
  lifecycle {
    prevent_destroy = true
  }
}