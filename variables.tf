##############################################################################
#
# Author: Logan Mancuso
# Created: 04.04.2024
#
##############################################################################

variable "datacenter_name" {
  description = "name of the cluster/datacenter"
  type        = string
}

### Datacenter Nodes ###
variable "nodes" {
  description = "default settings for additional nodes in the datacenter, netmask is /format"
  type = map(object({
    name     = string
    ip       = string
    domain   = string
    zfs_pool = string
    # its a bit annoying to do this but its useful. 
    # proxmox really likes /cidr notation and having the netmask
    # as a distinct variable allows more flexability in the code
    # this should match the netmask of the cluster network variable
    netmask      = string
    onboard_nics = list(string)
  }))
  default = {
    "primary" = {
      name         = "primary_node"
      ip           = "XXX.XXX.XXX.XXX"
      domain       = "domain.local"
      zfs_pool     = null
      netmask      = "YY"
      onboard_nics = ["eth0"]
    }
  }
}

variable "domain_name" {
  description = "local sub domain for cluster"
  type        = string
}
variable "dns_servers" {
  description = "dns servers to use in the datacenter"
  type        = map(string)
}

variable "networks" {
  description = "network definitions for you local network"
  type = map(object({
    # this notation has the form of an ipv4 addr these are just aliases in proxmox.
    cidr = string
    # this notation has the form of an ipv4 addr these are just aliases in proxmox.
    gateway = string
    # this notation has the form of /YY for an ipv4 addr these are just aliases in proxmox.
    netmask   = string
    comment   = string
    subdomain = string
    vlan      = number
  }))
  default = {
    "mgmt" = {
      cidr      = "XXX.XXX.XXX.XXX"
      gateway   = "XXX.XXX.XXX.X"
      netmask   = "YY"
      comment   = "this is the network for management of the cluster, this is your trusted network"
      subdomain = null
      vlan      = 0
    },
    "cluster" = {
      cidr      = "XXX.XXX.XXX.XXX"
      gateway   = "XXX.XXX.XXX.X"
      netmask   = "YY"
      comment   = "this is the network your cluster nodes will be connected to physically, if you dont have a vlan just set this to the same range as the management"
      subdomain = null
      vlan      = 0
    },
    "vpc-app" = {
      cidr      = "XXX.XXX.XXX.XXX"
      gateway   = "XXX.XXX.XXX.X"
      netmask   = "YY"
      comment   = "this is the app network for the vpc, this is where virtual resources will be deployed"
      subdomain = null
      vlan      = 0
    }
  }
}

variable "images" {
  description = "Images to pulldown for the datacenter, these are the operating systems supported by the datacenter."
  # Machine/LXC { os { version { url, sum, alg } } }
  # if you want to skip checksum checking just set sum and alg to null
  type = object({
    lxc = map(map(object({
      url          = string
      checksum     = string
      checksum_alg = string
    })))
    machine = map(map(object({
      url          = string
      checksum     = string
      checksum_alg = string
    })))
  })
  default = {
    lxc = {
      ubuntu = {
        # mantic-2310 = {
        #   url          = "http://download.proxmox.com/images/system/ubuntu-23.10-standard_23.10-1_amd64.tar.zst",
        #   checksum     = "91b92c717f09d2172471b4c85a00aea3",
        #   checksum_alg = "md5"
        # }
        noble-2404 = {
          url          = "http://download.proxmox.com/images/system/ubuntu-24.04-standard_24.04-2_amd64.tar.zst"
          checksum     = "4030982618eeae70854e8f9711adbd09",
          checksum_alg = "md5"
        }
      },
      opensuse = {
        leap-1505 = {
          url          = "http://download.proxmox.com/images/system/opensuse-15.5-default_20231118_amd64.tar.xz",
          checksum     = "18c8566b848c907ec74cee3a45348957",
          checksum_alg = "md5"
        }
      },
      debian = {
        bookworm-1207 = {
          url          = "http://download.proxmox.com/images/system/debian-12-standard_12.7-1_amd64.tar.zst",
          checksum     = "0b6959720ebd506b5ddb2fbf8780ca6c",
          checksum_alg = "md5"
        }
      }
    },
    machine = {
      ubuntu = {
        # mantic-2310 = {
        #   url          = "https://old-releases.ubuntu.com/releases/mantic/ubuntu-23.10-beta-live-server-amd64.iso",
        #   checksum     = "5e431c47de6c0616eae6d0db9ab1a3e75b95f848e99e9a8ef4165888e5fe18c2",
        #   checksum_alg = "sha256"
        # },
        noble-2404 = {
          url          = "https://old-releases.ubuntu.com/releases/noble/ubuntu-24.04-beta-live-server-amd64.iso",
          checksum     = "884667782ef3f3237695c68725f9f1f79d2d1cf2db94d3eb9d64a018341e87ee",
          checksum_alg = "sha256"
        }
      },
      opensuse = {
        leap-1506 = {
          url          = "https://download.opensuse.org/distribution/leap/15.6/iso/openSUSE-Leap-15.6-DVD-aarch64-Current.iso",
          checksum     = "6ecade658ef3e4dd7175176781f80fcd070250fe7e922f6240224ff810755ac6",
          checksum_alg = "sha256"
        }
      }
    }
  }
}