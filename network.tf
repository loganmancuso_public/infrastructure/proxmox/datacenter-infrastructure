##############################################################################
#
# Author: Logan Mancuso
# Created: 11.27.2023
#
##############################################################################

# terraform import proxmox_virtual_environment_network_linux_bridge.vmbr0 node_name:vmbr0
resource "proxmox_virtual_environment_network_linux_bridge" "vmbr0" {
  for_each   = var.nodes
  node_name  = each.value.name
  name       = "vmbr0"
  autostart  = true
  vlan_aware = true
  address    = "${each.value.ip}/${each.value.netmask}"
  gateway    = local.network_aliases["cluster"].gateway
  comment    = "default bridge interface"
  ports      = each.value.onboard_nics
  lifecycle {
    # if you destroy this resource it could break proxmox
    prevent_destroy = true
  }
}

### DNS Configuration ###

resource "proxmox_virtual_environment_dns" "preferred" {
  for_each  = var.nodes
  node_name = each.value.name
  domain    = var.domain_name
  servers   = [for ip in var.dns_servers : ip]
}

resource "proxmox_virtual_environment_hosts" "hosts" {
  for_each  = var.nodes
  node_name = each.value.name
  entry {
    address = "127.0.0.1"

    hostnames = [
      "localhost",
      "localhost.localdomain"
    ]
  }
  entry {
    address = each.value.ip

    hostnames = [
      "${each.value.name}.${each.value.domain}",
      "${each.value.name}"
    ]
  }
  # The following lines are desirable for IPv6 capable hosts
  entry {
    address = "::1"

    hostnames = [
      "ip6-localhost",
      "ip6-loopback"
    ]
  }
  entry {
    address = "fe00::0"

    hostnames = [
      "ip6-localnet"
    ]
  }
  entry {
    address = "ff00::0"

    hostnames = [
      "ip6-mcastprefix"
    ]
  }
  entry {
    address = "ff02::1"

    hostnames = [
      "ip6-allnodes"
    ]
  }
  entry {
    address = "ff02::2"

    hostnames = [
      "ip6-allrouters"
    ]
  }
  entry {
    address = "ff02::3"

    hostnames = [
      "ip6-allhosts"
    ]
  }
}