##############################################################################
#
# Author: Logan Mancuso
# Created: 04.04.2024
#
##############################################################################

## Datacenter ##
output "nodes" {
  description = "details for nodes in datacenter"
  value = {
    for key, value in var.nodes :
    key => {
      name           = var.nodes[key].name
      ip             = var.nodes[key].ip
      netmask        = var.nodes[key].netmask
      bridge_default = proxmox_virtual_environment_network_linux_bridge.vmbr0[key]
      directories    = local.directories
    }
  }
}

output "datacenter_keys" {
  description = "path in vault to datacenter_keys secret"
  value = {
    mount = local.vault_paths["infra"]
    name  = vault_kv_secret_v2.datacenter_keys.name
  }
}

## Prefixes ##
output "resource_prefix" {
  description = "proxmox id prefix for each resource"
  value = {
    template_lxc      = "10"
    template_instance = "20"
    lxc               = "11"
    instance          = "21"
    data_instance     = "9"
  }
}

## Users ##
output "operations_role" {
  description = "operations role"
  value       = proxmox_virtual_environment_role.operations.role_id
}

output "operations_user" {
  description = "operations user"
  value       = nonsensitive(proxmox_virtual_environment_user.operations.user_id)
}

## Security Groups ##
output "sg_vmdefault" {
  description = "Default SG for all vms's"
  value       = proxmox_virtual_environment_cluster_firewall_security_group.vm_default.name
}

## Networking ##
output "network_domain_name" {
  description = "Domain Name of the Cluster"
  value       = var.domain_name
}

output "network_dns" {
  description = "List of configured DNS servers"
  value       = var.dns_servers
}

output "networks" {
  description = "Network details"
  value = {
    for key, value in local.network_aliases :
    key => {
      id        = proxmox_virtual_environment_firewall_alias.networks[key].id
      name      = proxmox_virtual_environment_firewall_alias.networks[key].name
      ip        = split("/", proxmox_virtual_environment_firewall_alias.networks[key].cidr)[0]
      netmask   = split("/", proxmox_virtual_environment_firewall_alias.networks[key].cidr)[1]
      cidr      = proxmox_virtual_environment_firewall_alias.networks[key].cidr
      vlan      = value.vlan
      gateway   = value.gateway
      subdomain = value.subdomain
    }
  }
}

output "ipsets" {
  description = "ip sets available to the datacenter"
  value = {
    dns     = proxmox_virtual_environment_firewall_ipset.dns
    nodes   = proxmox_virtual_environment_firewall_ipset.nodes
    trusted = proxmox_virtual_environment_firewall_ipset.trusted
    vpc     = proxmox_virtual_environment_firewall_ipset.vpc_networks
  }
}

## LXC / Machine Images ##
output "images_lxc" {
  description = "lxc container images available in the datacenter"
  value = {
    for id, node in var.nodes : id => {
      for os, versions in var.images.lxc : os => {
        for version, details in versions : version => {
          id           = proxmox_virtual_environment_download_file.image_lxc["${id}_${os}_${version}"].id
          file_name    = proxmox_virtual_environment_download_file.image_lxc["${id}_${os}_${version}"].file_name
          datastore_id = proxmox_virtual_environment_download_file.image_lxc["${id}_${os}_${version}"].datastore_id
        }
      }
    }
  }
}

output "images_machine" {
  description = "machine images available in the datacenter"
  value = {
    for id, node in var.nodes : id => {
      for os, versions in var.images.machine : os => {
        for version, details in versions : version => {
          id           = proxmox_virtual_environment_download_file.image_machine["${id}_${os}_${version}"].id
          file_name    = proxmox_virtual_environment_download_file.image_machine["${id}_${os}_${version}"].file_name
          datastore_id = proxmox_virtual_environment_download_file.image_machine["${id}_${os}_${version}"].datastore_id
        }
      }
    }
  }
}