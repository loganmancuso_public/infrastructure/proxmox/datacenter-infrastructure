##############################################################################
#
# Author: Logan Mancuso
# Created: 07.30.2023
#
##############################################################################

## Provider ##

terraform {
  backend "http" {
    address = "https://gitlab.com/api/v4/projects/XXXXXXXX/terraform/state/dev"
  }
}