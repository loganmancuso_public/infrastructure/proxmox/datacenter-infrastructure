##############################################################################
#
# Author: Logan Mancuso
# Created: 04.04.2024
#
##############################################################################

## Network Aliases ##
resource "proxmox_virtual_environment_firewall_alias" "nodes" {
  for_each = var.nodes
  name     = each.value.name
  cidr     = each.value.ip
  comment  = "node id ${each.key}"
}

resource "proxmox_virtual_environment_firewall_alias" "networks" {
  for_each = local.network_aliases
  name     = each.key
  cidr     = "${each.value.cidr}/${each.value.netmask}"
  comment  = each.value.comment
}

## IP Sets ##
resource "proxmox_virtual_environment_firewall_ipset" "dns" {
  name    = "dns"
  comment = "preferred dns servers"
  dynamic "cidr" {
    for_each = var.dns_servers
    content {
      name    = cidr.value
      comment = cidr.key
    }
  }
  # lifecycle {
  #   ignore_changes = [ cidr ]
  # }
}

resource "proxmox_virtual_environment_firewall_ipset" "nodes" {
  name    = "cluster_nodes"
  comment = "nodes in the cluster"
  dynamic "cidr" {
    for_each = proxmox_virtual_environment_firewall_alias.nodes
    content {
      name    = cidr.value.name
      comment = cidr.value.comment
    }
  }
}

# this ipset is the default trusted networks in the infrastructure
# default case is mgmt and a defined vpn one if provided
resource "proxmox_virtual_environment_firewall_ipset" "trusted" {
  name    = "trusted_networks"
  comment = "default trusted subnets"

  cidr {
    name    = proxmox_virtual_environment_firewall_alias.networks["mgmt"].name
    comment = proxmox_virtual_environment_firewall_alias.networks["mgmt"].comment
  }
  dynamic "cidr" {
    for_each = local.is_vpn_network
    content {
      name    = cidr.value.name
      comment = cidr.value.comment
    }
  }
}

# create an ip set to classify the networks in the infrastructure
# this is all networks with the pattern vpc-*
resource "proxmox_virtual_environment_firewall_ipset" "vpc_networks" {
  name    = "vpc_networks"
  comment = "internal vpc subnets"

  dynamic "cidr" {
    for_each = tomap({
      for key, value in proxmox_virtual_environment_firewall_alias.networks :
      key => value if startswith(key, "vpc-")
    })
    content {
      name    = cidr.key
      comment = cidr.value.comment
    }
  }
}

## Datacenter Firewall Policy ##
resource "proxmox_virtual_environment_cluster_firewall" "dc_firewall_policy" {
  enabled       = true
  ebtables      = true
  input_policy  = "DROP"
  output_policy = "ACCEPT"
  log_ratelimit {
    enabled = false
    burst   = 10
    rate    = "5/second"
  }
  lifecycle {
    # if you destroy this resource it could break remote access to proxmox
    prevent_destroy = true
  }
}

## Datacenter Default Rules ##
resource "proxmox_virtual_environment_firewall_rules" "dc_default" {
  ## Inbound Rules ##
  rule {
    security_group = proxmox_virtual_environment_cluster_firewall_security_group.dc_default.name
    comment        = "datacenter security group"
  }
  # Default DROP Rule
  rule {
    type    = "in"
    action  = "DROP"
    comment = "default"
    log     = "alert"
  }
  ## Outbound Rules ##
  lifecycle {
    # if you destroy this resource it could break remote access to proxmox
    prevent_destroy = true
  }
}
